package maksi.chapter6

fun main(args: Array<String>) {
    var s:String? = "as"
    s = null

    val strLen = strLen(s)
    val let = s?.let { strLenNonnull(it) }
    println(strLen)

}

fun strLen(s: String?) = s?.length ?: -1
fun strLenNonnull(s: String) = s.length