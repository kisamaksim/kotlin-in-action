package maksi.chapter8

fun main(args: Array<String>) {

    val sum = {x: Int, y: Int ->  x + y}

    val sum1 = sum(1, 2)
    println(sum1)

    twoAndThree(sum)
    twoAndThree { first, second -> first * second }

    val listOf = listOf("maksim", "polly", "vitaly")
    listOf.joinToString(separator = " $") {it.toUpperCase()}


    val someF = makeLambda()
    someF("maksid")
    someF("winter is coming")
//    listOf.filter {  }.map{}

}

fun twoAndThree(function: (first: Int, second: Int) -> Int) {
    val functionResult = function.invoke(2, 3)
    println(functionResult)
}

fun findAlice (names: List<String>) {
    names.forEachStupid { if (it == "Alice") return }
}

inline fun List<String>.forEachStupid(doit: (String) -> Unit) {
    for (value in this) {
        doit(value)
    }
}

fun makeLambda(): (String) -> Unit {
    return { println(it) }
}

interface DoSmt<in T> {
    fun doSmt(value: T)
}