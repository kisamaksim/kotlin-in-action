package maksi.chapter3

import maksi.chapter3.pac.getFirst as fi

class Chapter3 {

    infix fun iDo(what: String) {
        println("i do $what")
    }
}

fun main(args: Array<String>) {
    val hashSetOf = hashSetOf(1, 2, 3, 4)
    val list = listOf("maksi", "poli", "vital")
    val hashMapOf = hashMapOf(1 to "maksi", 2 to "polly")

    println(hashMapOf)

    printSomeShit(b = "henlo", c = "max")

    val cp3 = Chapter3()
    cp3 iDo "swim"

    println("Maksi".getLast())
    println("Maksi".getLastAfterLast())
    println("Maksi".fi())


    println("@NotNull(message)".substringAfter("@").substringBefore("("))
    println("@NotNull".substringAfter("@").substringBefore("("))

    val regex = """@([a-zA-Z]*)\(.*|$""".toRegex()
    val matchEntire = regex.matchEntire("@NotNull(message)")
    if (matchEntire != null) {
        val (group1) = matchEntire.destructured
        println("a")
    }
    println("""${'$'}firstName""")

    val mak = """|||  ||
                 |||\/||
                 |||  ||
    """.trimMargin()

    println(mak)
}

fun String.getLast() = get(length - 1)

fun String.getLastAfterLast() = "" + getLast() + get(length-2)

