package maksi.chapter3.pac

fun String.getFirst() = get(0)