package maksi.chapter11

import java.lang.StringBuilder

fun main() {

    val result = buildString {
        with(it) {
            append("lalala")
            append("fafafa")
        }
    }
    println(result)

    val resultTwo = buildStringTwo {
        append("lolololo")
        append("fofofo")
    }

    val fucker = Fucker()
    fucker("max")
    fucker["max"]

    val fu = {value: String -> println(value)}
    fu("max")
}

fun buildString(builder: (sb: StringBuilder) -> Unit): String {
    val sb = StringBuilder()
    builder(sb)
    return sb.toString()
}

fun buildStringTwo(builder: StringBuilder.(Int) -> Unit) : String {
    val sb = StringBuilder()
    sb.builder(1)
    return sb.toString()
}

class Fucker() {
    operator fun invoke(who: String) {
        println("Fuck you $who")
    }

    operator fun get(who: String) {
        println("Fuck you $who from get")
    }

}