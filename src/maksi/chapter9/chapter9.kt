package maksi.chapter9

fun main() {

    val child = Child()
    doParStuff(child)


    val s = "aaaaaaaaaB"
    val split = s.split("a")
    println(split.size)

    val mutableListOf = mutableListOf("a", "v")
    addFakeNumber(mutableListOf)

    addNumberTwo(mutableListOf(1,2,34))
    addNumberTwo(mutableListOf(1.2,2.2,34.5))

    val dosm = object : DoSmt<Parent> {
        override fun doSmt(value: Parent) {
            value.doParentStuff()
        }
    }

    smtFun(Child(), dosm)

}

open class Parent {

    fun doParentStuff() {
        println("parent stuff")
    }
}

class Child : Parent() {

    fun doChildStuff() {
        println("child stuff")
    }
}

fun <T : Parent> doParStuff(value: T) {
    value.doParentStuff()
}

fun addNumber(list: MutableList<Any>) {
    list.add(42)
}

fun addNumberTwo(list: MutableList<Number>) {
    list.add(42)
}

fun <T> smtFun(value: T, doSmt: DoSmt<T>) {
    doSmt.doSmt(value)
}

interface DoSmt<in T> {
    fun doSmt(value: T)
}

fun addFakeNumber(list: List<Any>) {
    println(list[0])
}

class Holder<in T>() {

    fun doSmt(value: T) {
        println("blaHolder $value")
    }
}