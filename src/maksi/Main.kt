package maksi

import maksi.functions.printSomeText

class Main(val a: Int, val b: Int ) {
    val isMain: Boolean get() = a > b
}

data class Person(val name: String, var age: Int? = null)

enum class Creatures(val isHuman: Boolean) {
    MAXXX(true),
    SPOCK(false);


}

interface Inter
class A(): Inter {

    fun a() = "I am a"
}
class B: Inter {
    fun b() = "I am b"
}

fun test(tmp: Inter, tmpTwo: Inter) {
}


fun main (args: Array<String>) {
    val persons = listOf(Person("Poli"), Person("Maksi", 24))
    val oldestPerson = persons.maxBy { it.age ?: 0 }
    println(oldestPerson)


    val maksi = Person("Maxx")
    println(maksi)


    println("Say my name ${maksi.name}")

    maksi.age = 100

    val m = Main(5, 6)
    println(m.isMain)

    printSomeText("e boooooooy")

    val maxxx = Creatures.MAXXX

    println(whoIsIt(maxxx))

    for (i in 1..10) {
        println(i)
    }

    for (i in 1 until 10) {
        println(i)
    }

    for (i in 1..10 step 2) {
        println(i)
    }

    for (i in 10 downTo 1) {
        println(i)
    }

    val per = listOf(Person("Maksi"), Person("Polly"))
    for ((index, element) in per.withIndex()) {
        println("$index : $element")
    }
}

fun whoIsIt(creatures: Any) =
        when(creatures) {
            is Int -> println("int")
            is String -> println("string")
            else -> println("don't give a fuck about")
        }