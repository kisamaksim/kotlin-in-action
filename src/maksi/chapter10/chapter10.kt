package maksi.chapter10

fun main() {
    removeOne()
    val maks = Person("maks", 24)
    val javaClass = maks.javaClass.kotlin
    val kClass = maks::class
    println()

}

@Target(AnnotationTarget.FIELD, AnnotationTarget.FUNCTION)
annotation class RealG(val name: String)

data class Person(@RealG("gggg") val name: String, val age: Int) {

    @RealG("ooooog")
    fun doSmt() {
        println("do smt")
    }
}


@Deprecated(message = "don't use this", replaceWith = ReplaceWith("removeTwo()"))
fun removeOne() {}

fun removeTwo() {}