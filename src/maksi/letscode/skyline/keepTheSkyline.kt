package maksi.letscode.skyline

fun main() {
    val solution = Solution()
    // [3,0,8,4],[2,4,5,7],[9,2,6,3],[0,3,1,0]
    val init = arrayOf(intArrayOf(3, 0, 8, 4),
                        intArrayOf(2, 4, 5, 7),
                        intArrayOf(9, 2, 6, 3),
                        intArrayOf(0, 3, 1, 0))

    val result = solution.maxIncreaseKeepingSkyline(init)

    println(result)

}

class Solution {
    fun maxIncreaseKeepingSkyline(grid: Array<IntArray>): Int {
        // определить максимальное число для просмотра сверху и снизу
        // определить максимальное число для просмотра справа и слева
        val transpose = transpose(grid)
        val topToBottom = Array(grid.size) { transpose[it].max()!! }
        var maxIncrease = 0
        for ((i, value) in grid.withIndex()) {
            for (j in value.indices) {
                val maxLeftRight = value.max()
                val building = grid[i][j]
                maxIncrease += if (topToBottom[j] >= maxLeftRight!!) {
                    maxLeftRight - building
                } else {
                    topToBottom[j] - building
                }
            }
        }
        return maxIncrease
    }

    private fun transpose(array: Array<IntArray>): Array<IntArray> {
        val result = Array(array.size) { IntArray(array.size) }
        for ((i, inner) in array.withIndex()) {
            for (j in inner.indices) {
                result[i][j] = array[j][i]
            }
        }
        return result
    }

    fun defangIPaddr(address: String): String {
        return address.replace(".", "[.]")
    }
}