package maksi.chapter5

import java.io.File

data class Person(val name: String, val age: Int)

fun main(args: Array<String>) {
    val persons = listOf(Person("maksi", 24), Person("polly", 26))
    val oldestPerson = persons.maxBy { it.age }
    println("oldest person is ${oldestPerson?.name}")

    println(persons.joinToString("$", "<|", "|>") {it.name})

    val peoples = mapOf(1 to "max", 2 to "polly")
    val stringPeoplesMapKeys = peoples.mapKeys { it.key.toString() }
    println("")

}

fun File.isInsideHiddenDirectory() = generateSequence(this) { it.parentFile }.any { it.isHidden }