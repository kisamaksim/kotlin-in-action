package maksi.chapter4

import javax.print.attribute.IntegerSyntax

interface Fuckoffable {
    fun fuckoff() { println("I am fuckoffable")}
}

interface ChildFuckoffable {
    fun fuckoff() { println("I am child fuckoffable")}
}

class Maks : Fuckoffable {

    override fun fuckoff() {
        println("fuck off boi")
    }

}
private class HiddenLeaf {

}

class Vital : Fuckoffable, ChildFuckoffable {
    override fun fuckoff() {
        super<Fuckoffable>.fuckoff()
        super<ChildFuckoffable>.fuckoff()
    }
}

sealed class A {
    class B(val value: String) : A()
    class C: A()
}

open class Parent {

    lateinit var a: String

    constructor(a:String) {
        this.a = a;
    }
}

class Child : Parent {
    constructor(a: String) : super(a)

}

fun main(args: Array<String>) {


    val m = Maks()
    m.fuckoff()

    val v = Vital()
    v.fuckoff()

    val hiddenLeaf = HiddenLeaf()

    val b = A.B("maksi")
    val c = A.C()

    val simpleUser = SimpleUser("kisamaksim")
    val simpleUserTwo = SimpleUserTwo()

    val fieldParty = FieldParty(10)
    val client = Client("maks")
    client.secondName = "grom"

    val copy = client.copy()
    println()
}


interface User {
    val nickname: String
}

class SimpleUser(override val nickname: String) : User
class SimpleUserTwo: User {
    override val nickname: String
        get() = "nickname"
}

class FieldParty(val counter: Int) {
    lateinit var name: String

    val nameTwo: String
        get() = name

}

data class Client(val name: String) {
    lateinit var secondName:String
}