package maksi.chapter7

import kotlin.reflect.KProperty

fun main(args: Array<String>) {
    val pointOne = Point(10, 10)
    val pointTwo = Point(20, 20)
    println(pointOne + pointTwo)
    println(pointOne * 1.8)

    val closedRange = Person(10)..Person(15)
    println(Person(12) in closedRange)
    for (p in closedRange) {
        println(p)
    }

    val (firstPart, secondPart) = separate("poli.maksi")
    println(firstPart)
    println(secondPart)

    val person2 = Person2()
    person2.name

    person2.surName = "Max"
    val surName = person2.surName
    println(surName)
}

data class Point(val x: Int, val y: Int) {
    operator fun plus(other: Point) = Point(x + other.x, y + other.y)

    fun toUp(s: String) = s.toUpperCase()
}

operator fun Point.times(scale: Double) = Point((x * scale).toInt(), (y * scale).toInt())

fun Point.toUp(s: String) = s

data class Person(val age: Int) : Comparable<Person> {
    override fun compareTo(other: Person): Int {
        return compareValuesBy(this, other, Person::age)
    }
}

operator fun ClosedRange<Person>.iterator(): Iterator<Person> =
    object : Iterator<Person> {
        var current = start;

        override fun hasNext(): Boolean = current < endInclusive

        override fun next(): Person {
            current = Person(current.age + 1)
            return current
        }
    }

data class NameComponent(val firstPart:String, val secondPart:String)

fun separate(s: String): NameComponent {
    val (first, second) = s.split('.',limit =  2)
    return NameComponent(first, second)
}

class Person2() {
    val name by lazy { whatIsName() }
    var surName by MyCustomDelegate()

    private fun whatIsName(): String {
        println("вызвана делегатом")
        return "maksi"
    }
}

class MyCustomDelegate {

    lateinit var surName: String

    operator fun setValue(o: Person2, prop: KProperty<*>, value: String) {
        println("SetValueDelegateMethodBOooooy name: ${prop.name} | return type: ${prop.returnType}")
        surName = value
    }

    operator fun getValue(o: Person2, prop: KProperty<*>): String {
        println("GetValueDelegateMethodBOooooy ${prop.name}")
        return surName;
    }
}